package Enumerations

object GreekPlanetsEnum extends Enumeration {
  val Mercury:Value = Value(1, "Hermes")
  val Venus:Value = Value(2, "Aphrodite")
  //Aggregation.Fun Fact: Tellus is Roman for (Mother) Earth
  val Earth:Value = Value(3, "Gaia")
  val Mars:Value = Value(4, "Ares")
  val Jupiter:Value = Value(5, "Zeus")
  val Saturn:Value = Value(6, "Cronus")
  val Uranus:Value = Value(7, "Ouranus")
  val Neptune:Value = Value(8, "Poseidon")
  val Pluto:Value = Value(9, "Hades")
}
