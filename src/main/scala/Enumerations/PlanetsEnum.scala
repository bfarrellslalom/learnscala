/*
  TODO: Additional Reading: https://underscore.io/blog/posts/2014/09/03/enumerations.html

  TODO: Add some examples based on the article above.
 */
package Enumerations

object PlanetsEnum extends Enumeration {
  //can be set as one line.
  val Mercury, Venus, Earth, Mars, Jupiter, Saturn, Uranus, Neptune, Pluto: Value  = Value
}
