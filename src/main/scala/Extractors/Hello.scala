package Extractors

//An extractor object is an object with an unapply method.
// Whereas the apply method is like a constructor which takes arguments and creates an object,
// the unapply takes an object and tries to give back the arguments.
object Hello {
  def apply(x: String): String = "Hello %s".format(x)
  def unapply(x: String): Option[String] = {
    if (!x.isEmpty)
      Some("Hello again, %s".format(x))
    else None
  }
}
