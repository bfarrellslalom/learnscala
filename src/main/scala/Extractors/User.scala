package Extractors

trait User {
  def name: String
  def score: Int
}
