package Extractors

object Methods {
  def myFamily(person: Person): String = {
    person match {
      case Person(_, "FamilyName") => "Hello %s".format(person.firstName)
      case _ => "Hello Friend"
    }
  }

  def getUserDesc(user: User): String = {
    user match {
      case FreeUser(name, _, p) =>
        if (p > 0.75) name + ", what can we do for you today?" else "Hello " + name
      case PremiumUser(name, _) => "Welcome back, dear " + name
      case _ => "Hello, %s".format(user.name)
    }
  }
}
