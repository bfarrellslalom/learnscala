package Extractors

class PremiumUser(val name: String, val score: Int) extends User
object PremiumUser {
  def unapply(user: PremiumUser): Option[(String, Int)] = Some((user.name, user.score))
}