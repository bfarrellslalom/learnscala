package Extractors

class DefaultUser(val name: String, val score: Int) extends User
object DefaultUser {
  def unapply(user: DefaultUser): Option[(String, Int)] = Some(user.name, user.score)
}