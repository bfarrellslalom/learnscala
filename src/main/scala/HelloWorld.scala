class HelloWorld {
  def SayHello(name:String): String = {
    s"Hello $name"
  }
}
