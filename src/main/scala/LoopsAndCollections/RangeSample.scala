package LoopsAndCollections

import scala.collection.mutable

class RangeSample {
  def getRange = Range(0, 10)

  def getRangeInc = Range(2, 10, 3)

  def rangeLoop:List[Int] = {
    val o = mutable.MutableList[Int]()
    for(i <- getRangeInc) {
      o += i
    }
    o.toList
  }
}
