package LoopsAndCollections

class LoopNotes {
  def learnYield: List[(Int, Int)] = {
    val xValues = 1 to 4
    val yValues = 1 to 2
    (for {
          x ← xValues
          y ← yValues
        } yield (x, y))(collection.breakOut)
  }

  //I like this example for enumerating a list of lists
  //also useful for the filters
  def listofLists: List[Int] = {
    val nums = List(List(1), List(2), List(3), List(4), List(5))

    for {
      numList ← nums
      num ← numList
      if num % 2 == 0
    } yield num
  }
}
