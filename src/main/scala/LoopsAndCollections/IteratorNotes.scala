package LoopsAndCollections

class IteratorNotes {
  def getNext(a: List[Int]): Int = {
    a.iterator.next
  }

  def getGrouped(a: List[Int]):Iterator[Int]#GroupedIterator[Int] = {
    a.iterator grouped 3
  }

  def getSliding (a: List[Int]):Iterator[Int]#GroupedIterator[Int] = {
    a.iterator sliding 3
  }

  def getSlidingStep (a: List[Int]):Iterator[Int]#GroupedIterator[Int] = {
    a.iterator.sliding(3, 3)
  }

  def getZip (a: List[Int], b:List[String]):List[(Int, String)] = {
    a zip b
  }

  def getZipAll (a: List[Int], b:List[String]):List[(Int, String)] = {
    a zipAll (b, -1, "?") //-1 and ? are the default values if there is a mismatch
  }

  def getZipWithIndex (a: List[String]):List[(String, Int)] = {
    a.zipWithIndex
  }
}
