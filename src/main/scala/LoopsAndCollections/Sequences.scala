package LoopsAndCollections

class Sequences {
  def reverseMap(): Seq[String] = {
    val s = Seq("hello", "world")
    s map {_.reverse}
  }
}
