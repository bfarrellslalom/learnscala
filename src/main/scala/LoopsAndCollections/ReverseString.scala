package LoopsAndCollections

/*
Write a function that takes a string as input and returns the string reversed.

Example:
Given s = "hello", returns characters in reverse.
 */

class ReverseString {
  def reverseString(s: String): String = {
    s.toCharArray.reverse.mkString
  }
}
