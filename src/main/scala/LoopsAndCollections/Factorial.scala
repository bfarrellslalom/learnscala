package LoopsAndCollections

import scala.annotation.tailrec

//Tail recursion example
//Effectily the annotation aggregates using result, and run more like a loop and not as a recursive call
object Factorial {
  def factorial(n: Int): Int = {
    @tailrec
    def iter(x: Int, result: Int): Int =
      if (x == 1) result
      else iter(x - 1, result * x)

    iter(n, 1)
  }
}
