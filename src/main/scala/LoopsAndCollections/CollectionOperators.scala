package LoopsAndCollections

import scala.collection.mutable

class CollectionOperators {
  def addNewAndExisting[T](list: mutable.MutableList[T], item: T) : mutable.MutableList[T] = list += item

  def addNewOnly[T](list: mutable.MutableList[T], item: T) : mutable.MutableList[T] = list ++ List(item)

  def union1[T](a: Set[T], b: Set[T]) : Set[T] = a | b
  def union2[T](a: Set[T], b: Set[T]) : Set[T] = a union b

  def intersect[T](a: Set[T], b: Set[T]) : Set[T] = a intersect b

  def diff1[T](a: Set[T], b: Set[T]):Set[T] = a &~ b
  def diff2[T](a: Set[T], b: Set[T]):Set[T] = a diff  b
}
