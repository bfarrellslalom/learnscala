/*
  Lesson Reference - https://www.scala-exercises.org/std_lib/traits
 */
package Traits

object TraitComposite {
  //how to mix traits to a new composite,
  def getComposite1 = new TraitA with TraitB

  //note that it will error if traits have identity property/method names
  //In this example, the first trait requires the second to be instantiated
  def getComposite2 = new TraitRequiresB with TraitB
}
