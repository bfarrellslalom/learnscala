package Traits

trait TraitRequiresB {
  //val a = new TraitRequiresB  //***does not compile!!!***
  //Self types make the listed trait required
  //val a = new TraitRequiresB with TraitB //Does compile!
  self: TraitB => def requireId = 10
}
