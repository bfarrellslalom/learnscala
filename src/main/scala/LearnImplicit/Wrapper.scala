package LearnImplicit

object Wrapper {
  implicit object StringMonoid extends Monoid[String] {
    def add(x: String, y: String): String = x concat y

    def unit: String = ""
  }

  implicit object IntMonoid extends Monoid[Int] {
    def add(x: Int, y: Int): Int = x + y

    def unit: Int = 0
  }

  implicit def intWrapper(value: Int): KoanIntWrapper = new KoanIntWrapper(value)
  implicit def intToDigit(value: Int): IntToDigitArray = new IntToDigitArray(value)

  def sum[A](xs: List[A])(implicit m: Monoid[A]): A =
    if (xs.isEmpty) m.unit
    else m.add(xs.head, sum(xs.tail))
}
