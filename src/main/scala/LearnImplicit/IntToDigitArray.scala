package LearnImplicit

class IntToDigitArray(val number: Int) {
  def split: List[Int] = (Stream.iterate(number)(_/10)takeWhile(_!=0)map(_%10) toList) reverse
}
