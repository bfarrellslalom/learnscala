package LearnImplicit

abstract class Monoid[A] extends SemiGroup[A] {
  def unit: A
}
