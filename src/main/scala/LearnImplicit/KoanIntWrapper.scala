package LearnImplicit

class KoanIntWrapper(val original: Int) {
  def isOdd: Boolean = original % 2 != 0
}
