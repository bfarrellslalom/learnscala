package Comparison

class ByName {
  def calc(x: () => Int): Either[Throwable, Int] = {
    try {
      Right(x())
    } catch {
      case b: Throwable => Left(b)
    }
  }

  def calc2(x: => Int): Either[Throwable, Int] = {
    try {
      Right(x)
    } catch {
      case b: Throwable => Left(b)
    }
  }

  object PigLatinizer {
    def apply(x: ⇒ String): String = {
      x.tail + x.head + "ay"} //tail prints all after frist character, head is the first character
  }
}
