package Comparison

//noinspection ScalaUnusedSymbol
class Matches {
  def simpleMatch(x: Int): String = x match {
    case 1 => "One"
    case 2 => "Two"
    case _ => "Many"
  }

  def patternMatch: Int = {
    val stuff = "blue"

    stuff match {
      case "red" => 1
      case "blue" => 2
      case "green" => 3
      case _ => 0
    }
  }

  def complexResult(x: String): (Int, Int, Int) = x match {
    case "red" ⇒ (255, 0, 0)
    case "green" ⇒ (0, 255, 0)
    case "blue" ⇒ (0, 0, 255)
    case _ ⇒ (0, 0, 0)
  }

  def complexMatch(exp: Any): String = exp match {
    case ("Value1", "Value2") => "Found"
    case _ => "Not Found"
  }

  def wildCard(exp: Any): String = exp match {
    case ("Value1", "Value2") => "Found"
    case ("Value3", _) => "Found"
    case _ => "Not Found"
  }

  def substitute(a: String, b: String): String = (a, b) match {
    case ("Value1", "Value2") => "Found"
    case ("Value3", `b`) => b
    case _ => "Not Found"
  }

  def backquote(i: Int, j: Int): Boolean = j match {
    case `i` ⇒ true
    case _ ⇒ false
  }

  def list(ints: List[Int]): Int = ints match {
    case x :: xs => xs.head
    case _ => 0
  }

  def list2(ints: List[Int]): Int = ints match {
    case x :: y :: xs => y
    case _ => 0
  }

  def list3(ints: List[Int]): Int = ints match {
    case x :: y :: Nil => 1
    case _ => 0
  }

  def list4(ints: List[Int]): Any = ints match {
    case x :: y :: z :: tail => tail
    case _ => 0
  }
}
