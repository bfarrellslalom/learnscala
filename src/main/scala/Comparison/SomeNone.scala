package Comparison

class SomeNone {
  def getNumber: Option[Int] = {
    Some(3)
  }
  def getNoNumber: Option[Int] = {
    None
  }
}
