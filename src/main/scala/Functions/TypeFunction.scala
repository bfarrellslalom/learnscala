package Functions

object TypeFunction {
  type Result = Either[String, (Int, Int)]
  def divide(dividend: Int, divisor: Int): Result =
    if (divisor == 0) Left("Division by zero")
    else Right((dividend / divisor, dividend % divisor))
}
