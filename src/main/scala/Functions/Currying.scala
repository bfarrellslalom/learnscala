package Functions

/**
 * Tutorial and real-world example on currying in Scala:
 * https://lukajcb.github.io/blog/scala/2016/03/08/a-real-world-currying-example.html
 */
class Currying {
  def curry[A, B, C](f: (A, B) => C): A => B => C = a => b => f(a, b)

  def f(a: Int, b: Int): Int = a + b

  def g(a: Int)(b: Int): Int = a + b

  def add(a: Int): Int => Int = { b: Int => a + b }

  def myCurry: Int => Int => Int = (f _).curried

  def hello: String => String => String = (sup _).curried
  def sup(greeting: String, name: String): String = greeting + ", " + name
}