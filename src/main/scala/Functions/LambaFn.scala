package Functions

//noinspection TypeAnnotation
class LambaFn {
  def lambda1 = (a: Int, b: Int) => a + b
  //name: output type (function input) &return type = function input => actual code
  def lambda1Proper: (Int, Int) => Int = (a: Int, b: Int) => a + b

  def lambda2 = {a: Int => a + a} //can this support more than 1 param?
  def lambda2Proper: Int => Int = {a: Int => a + a} //can this support more than 1 param?

  private val l3 = (a: Int, b: Int) => a + b
  def lambda3 = l3
  def lambda3Proper: (Int, Int) => Int = l3

  def lambda4 = new Function2[Int, Int, Int] {
    def apply(v1: Int, v2: Int) : Int = v1 + v2
  }
  def lambda4Proper: (Int, Int) => Int = (v1: Int, v2: Int) => v1 + v2

  def lambda5(a: Int, b: Int):Int = a + b

}
