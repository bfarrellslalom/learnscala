package Functions

//Using a trait as a function
trait TraitFunction [A, B]{
  def apply(x: A): B
}
