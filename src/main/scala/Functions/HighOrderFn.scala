package Functions

class HighOrderFn {
  def makeUpper(xs: List[String]): List[String] = xs map {
    _.toUpperCase
  }

  def makeWhatEverYouLike(xs: List[String], sideEffect: String ⇒ String) : List[String] =
    xs map sideEffect
}
