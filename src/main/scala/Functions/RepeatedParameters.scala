package Functions

object RepeatedParameters {
  def repeatedParameterMethod(x: Int, y: String, z: Any*): String = {
    "%d %ss can give you %s".format(x, y, z.mkString(", "))
  }

  def helloPeople(p: Any*): String = {
    "Hello %s!".format(p.mkString(", "))
  }
}
