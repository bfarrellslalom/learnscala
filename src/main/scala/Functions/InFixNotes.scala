package Functions

class InFixNotes {
/*
Postfix operators have lower precedence than infix operators, so:

foo bar baz means foo.bar(baz).
foo bar baz bam means (foo.bar(baz)).bam
foo bar baz bam bim means (foo.bar(baz)).bam(bim).
 */

  //TODO: Need to play around with the above more and check out cases where there are duplicate properties


  //Other
  def unary_+ = "on"
  def unary_- = "off"

  case class Person(name: String) {
    def loves(person: Person) = new Loves(this, person)
  }
  class Loves[A, B](val a: A, val b: B)

  def announceCouple(couple: Person Loves Person): String =
    //Notice our type: Person loves Person!
    couple.a.name + " is in love with " + couple.b.name

}
