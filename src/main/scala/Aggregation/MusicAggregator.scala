package Aggregation

object MusicAggregator {
  def symbolDuration(symbol: Symbol): Duration =
    symbol match {
      case Note(duration, _) => duration
      case Rest(duration) => duration
    }
  def duration(duration: Duration): Float = duration match {
    case Quarter => .25f
    case Half => .5f
    case Whole => 1f
  }

  def getDuration(symbol: Symbol): Float = {
    duration(symbolDuration(symbol))
  }
}
