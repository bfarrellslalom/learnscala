package Aggregation

sealed trait Duration
case object Whole extends Duration
case object Half extends Duration
case object Quarter extends Duration