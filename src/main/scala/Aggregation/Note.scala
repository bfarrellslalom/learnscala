package Aggregation

abstract case class Note(duration: Duration, octave: Int) extends Symbol {}

//Note cases classes can only be inherted by a class
class NoteA(duration: Duration, octave: Int) extends Note(duration, octave) {}
class NoteB(duration: Duration, octave: Int) extends Note(duration, octave) {}
class NoteC(duration: Duration, octave: Int) extends Note(duration, octave) {}
class NoteD(duration: Duration, octave: Int) extends Note(duration, octave) {}
class NoteE(duration: Duration, octave: Int) extends Note(duration, octave) {}
class NoteF(duration: Duration, octave: Int) extends Note(duration, octave) {}
