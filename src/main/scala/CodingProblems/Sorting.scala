package CodingProblems

class Sorting {
  def mergeSort(l: List[Int]): List[Int] = l.length match {
        case x if x < 2 => l
        case _ => {
            val (left, right) = l.splitAt(l.length / 2)
            merge(mergeSort(left), mergeSort(right));
        } 
  }

  //@annotation.tailrec
  private def merge(left: List[Int], right: List[Int]): List[Int] = (left, right) match {
    case (_, Nil) => left
    case (Nil, _) => right
    case (leftHead :: leftTail, rightHead :: rightTail) =>
        if (leftHead < rightHead) leftHead::merge(leftTail, right)
        else rightHead :: merge(left, rightTail)
  }
}