package CodingProblems

/*
Input: "A man, a plan, a canal: Panama"
Output: true
 */
class ValidPalindrome {
  def isPalindrome(s: String): Boolean = {
    val sa = s.replaceAll("[^A-Za-z0-9]", "").toLowerCase().toCharArray
    val rsa = sa.reverse

    for(i <- 0 until sa.length) {
      if(sa(i) != rsa(i)) return false
    }

    true
  }
}
