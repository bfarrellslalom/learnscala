package CodingProblems.Helpers

case class TreeNode[A](value: A) {
  var left: TreeNode[A] = _
  var right: TreeNode[A] = _
}
