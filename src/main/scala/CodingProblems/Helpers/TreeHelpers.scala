package CodingProblems.Helpers

object TreeHelpers {
  //Level Order
  def getTreeFromArray[A](a: Array[Option[A]]): TreeNode[A] = Option(a) match {
    case None => null
    case Some(Array()) => null
    case _ => insertLevelOrder(a.map(convertToNode))
  }

  private def convertToNode[A](item: Option[A]):TreeNode[A] = item match {
    case None => null
    case Some(x) => TreeNode[A](x)
  }

  private def insertLevelOrder[A](nodes: Array[TreeNode[A]]):TreeNode[A] = {
    //Only loop through half, as the bottom
    for(i <- 0 to nodes.length / 2; if nodes(i) != null) {
      //i is parent node, and 2*i+1=left, and 2*i+2=right
      if (2 * i + 1 < nodes.length) nodes(i).left = nodes(2 * i + 1)
      if (2 * i + 2 < nodes.length) nodes(i).right = nodes(2 * i + 2)
    }
    nodes(0)
  }
}
