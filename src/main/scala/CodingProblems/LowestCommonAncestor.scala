/*
Given a binary tree, find the lowest common ancestor (LCA) of two given values in the tree.

According to the definition of LCA on Wikipedia: “The lowest common ancestor is defined
between two nodes v and w as the lowest node in T that has both v and w as descendants (
where we allow a node to be a descendant of itself).”

Given the following binary search tree:

Example 1
Input: root = [6,2,8,0,4,7,9,null,null,3,5], p = 2, q = 8
Output: 6
Explanation: The LCA of nodes 2 and 8 is 6.

Example2
Input: root = [6,2,8,0,4,7,9,null,null,3,5], p = 2, q = 4
Output: 2
Explanation: The LCA of nodes 2 and 4 is 2, since a node can be a descendant of itself
             according to the LCA definition.

Note - Assume the value in a node is unique, allowing you to use the value for comparison
 */
package CodingProblems

import CodingProblems.Helpers.TreeNode

object LowestCommonAncestor {
  def lowestCommonAncestor(root: TreeNode[Int], n1:Int, n2:Int): TreeNode[Int] = (root, n1, n2) match {
    case _ if !hasNode(root, n1) => null
    case _ if !hasNode(root, n2) => null
    case _  => findLCA(root, n1, n2)
  }

  //Moved the found logic into a function, just to see how far I can break down logic
  private def isLCA(root: TreeNode[Int], n1: Int, n2: Int): Boolean = {
    ( n1 == root.value | n2 == root.value
      || n1 > root.value && n2 < root.value
      || n1 < root.value && n2 > root.value
    )
  }
  //Find the node for n1, or stop and return current node if n1/n2 diverge
  @annotation.tailrec
  private def findLCA(root: TreeNode[Int], n1: Int, n2: Int): TreeNode[Int] = root match {
    case _ if isLCA(root, n1, n2) => root
    case _ if n1 > root.value => findLCA(root.right, n1, n2)
    case _ if n1 < root.value => findLCA(root.left, n1, n2)
  }

  @annotation.tailrec
  private def hasNode(root: TreeNode[Int], n: Int): Boolean = root match {
    case _ if root == null => false
    case _ if n == root.value => true
    case _ if n > root.value => hasNode(root.right, n)
    case _ if n < root.value => hasNode(root.left, n)
  }
}
