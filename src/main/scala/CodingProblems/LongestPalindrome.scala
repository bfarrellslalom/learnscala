/*
Given a string s, find the longest palindromic substring in s. You may assume that the maximum length of s is 1000.

Example 1:

Input: "babad"
Output: "bab"
Note: "aba" is also a valid answer.
Example 2:

Input: "cbbd"
Output: "bb"
 */
package CodingProblems

object LongestPalindrome {
  //This is o(n^2) time, and o(n^2) space
  def getLongestPalindrome(s: String): String = {
    if(s == null) return ""

    val n = s.length()
    var res: String = ""
    val dp: Array[Array[Boolean]] = Array.ofDim[Boolean](n, n)

    for {i <- n - 1 to 0 by -1}
      for (j <- i until n) {
        //checks if we can form a palidrome string for s[i ... j] where end characters
        //are equal, and inner characters by 1 are a palindrome.
        dp(i)(j) = s.charAt(i) == s.charAt(j) && (j - i < 3 || dp(i + 1)(j - 1))

//        //uncomment for visualation
//        println("Outer:" + s.substring(i, j + 1) + "|" + dp(i)(j) + "|i=" + i + ",j=" + j +
//          " | Inner:" + (j - i < 3 || dp(i+1)(j-1)))

        //check if we found a palindrome, and it is longer than the last one
        if (dp(i)(j) && (res == null || j - i + 1 > res.length())) {
          res = s.substring(i, j + 1)
        }
      }

    res
  }
}
