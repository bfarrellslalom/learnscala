/*
Given a binary tree, return the tree in level order traversal of its nodes' values.
ie, from left to right, then right to left for the next level and alternate between).

For example:
Given binary tree [3,9,20,null,null,15,7]
Visua
        3
    9        20
null null  15   7

Returns
[[3], [20,9], [15,7]]

 */
package CodingProblems

import CodingProblems.Helpers.TreeNode

import scala.collection.mutable
import scala.collection.mutable.ListBuffer

object TreeLevelOrderTraversal {
  //Both implementations are O(n) time, and O(log(n)) space
  def levelOrder(root: TreeNode[Int]): List[Int] = root match {
    case null => Nil
    //Wrapper to swap between methodologies
    case _ => levelOrderTailRecursive(root)
    //case _ => levelOrderLoop(root)
    //case _ => levelOrderLoopFold(root)
  }

  //noinspection ScalaUnusedSymbol
  private def levelOrderLoop(root: TreeNode[Int]): List[Int] = {
    val currentLevel = new mutable.Queue[TreeNode[Int]]()
    val prevLevel = new mutable.Queue[TreeNode[Int]]()
    var output = ListBuffer(List(root.value))
    prevLevel.enqueue(root)

    while(prevLevel.nonEmpty) {
      //Store all elements for next level in currentStack
      while(prevLevel.nonEmpty) {
        val temp = prevLevel.dequeue()
        if (temp.left != null) currentLevel.enqueue(temp.left)
        if (temp.right != null) currentLevel.enqueue(temp.right)
      }

      //add items to array from current and move them to prevStack
      var level = ListBuffer[Int]()
      while(currentLevel.nonEmpty) {
        val temp = currentLevel.dequeue()
        prevLevel.enqueue(temp)
        level += temp.value
      }
      if(level.nonEmpty)
        output += level.toList
    }
    output.toList.flatten
  }

  //noinspection ScalaUnusedSymbol
  private def levelOrderLoopFold(root: TreeNode[Int]): List[Int] = {
    var next = List(root)
    val result = mutable.MutableList[List[Int]]()

    def helper(current: List[TreeNode[Int]]): List[TreeNode[Int]] = {
      if(current.isEmpty) null
      else {
        current.foldLeft(List.empty[TreeNode[Int]]) {
          case (list, node) if node.left == null && node.right == null => list
          case (list, node) if node.left == null => list :+ node.right
          case (list, node) if node.right == null => list :+ node.left
          case (list, node) => list :+ node.left :+ node.right
        }
      }
    }

    while(next != null) {
      if(next.nonEmpty)
        result += next.map(_.value)

      next = helper(next)
    }

    result.toList.flatten
  }

  //This will NOT cause a stack overflow because of the tailrec annotation
  //See http://blog.richdougherty.com/2009/04/tail-calls-tailrec-and-trampolines.html
  private def levelOrderTailRecursive(root: TreeNode[Int]): List[Int] = {
    @annotation.tailrec
    def helper(current: List[TreeNode[Int]], res: List[List[Int]]): List[List[Int]] = current match {
      case Nil => res
      case _ => helper(getNextLevel(current), res :+ current.map(_.value))
    }

    def getNextLevel(current: List[TreeNode[Int]]): List[TreeNode[Int]] = {
      current.foldLeft(List.empty[TreeNode[Int]]) {
        case (list, node) if node.left == null && node.right == null => list
        case (list, node) if node.left == null => list :+ node.right
        case (list, node) if node.right == null => list :+ node.left
        case (list, node) => list :+ node.left :+ node.right
      }
    }

    helper(List(root), Nil).flatten
  }
}

