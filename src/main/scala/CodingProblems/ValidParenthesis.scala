package CodingProblems

import scala.collection.mutable

/*
Given a string containing just the characters '(', ')', '{', '}', '[' and ']', determine if the input string is valid.

An input string is valid if:

Open brackets must be closed by the same type of brackets.
Open brackets must be closed in the correct order.
Note that an empty string is also considered valid.
 */

class ValidParenthesis {
  def isValid(s: String): Boolean = {
    var stack = new mutable.ArrayStack[Char]

    for(c <- s.toCharArray) {
      if(c == '(' | c == '[' | c == '{')
        stack += c
      else {
        val p = stack.pop()
        if(c == ')' && p != '(') return false
        if(c == ']' && p != '[') return false
        if(c == '}' && p != '{') return false
      }

    }

    stack.isEmpty
  }
}
