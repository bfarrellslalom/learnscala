package CodingProblems

import scala.collection.mutable

/*
Given an array of integers, return indices of the two numbers such that they add up to a specific target.
You may assume that each input would have exactly one solution, and you may not use the same element twice.

Example:
Given numbers = [2, 7, 11, 15], target = 9,
Because numbers[0] + numbers[1] = 2 + 7 = 9,
return [0, 1].
 */

class TwoSum {
  def twoSum(numbers: Array[Int], target: Int): Array[Int] = {
    var hs = mutable.Set[Int]()

    for(n <- numbers) {
      if (hs.add(target - n))
        return Array(n, target - n) //Question: Is return a good practice in scala? I have read it usually isn't
      else
        hs += target - n
    }
    Array(0,0)
  }
}