package Composition

object Simple {
  def singleComp1[A, B](f: A => B): A => B = a => f(a)
  def singleComp2[input, output](f: input => output): input => output = num => f(num)

  def duoComp1[A, B, C](f: B => C, g: A => B): A => C = a => f(g(a))
  def duoComp2[input, funcB, output](f: funcB => output, g: input => funcB): input => output = a => f(g(a))

  def twoParamComp[param1, param2, funcA, output](f: funcA => output, g: param1 => param2 => funcA): (param1, param2)
    => output = (a, b) => f(g(a)(b))

  def twoParamCompCrazy[param1, param2, param3, funcA, output]
    (f: funcA => param3 => output, g: param1 => param2 => funcA):
    (param1, param2, param3) => output = (a, b, c) => f(g(a)(b))(c)


  def double(a: Int):Int = a * 2
  def triple(a: Int):Int = a * 3
  def half(a: Int):Int = a / 2
  def addTwo(a: Int):Int = a + 2

  def add(a: Int)(b: Int):Int = a + b
  def multi(a: Int)(b: Int):Int = a * b
}
