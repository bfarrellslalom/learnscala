package Composition

class PartFunction extends {
  def evenOdder1(number: Int):Int = {
    val doubleEvens: PartialFunction[Int, Int] =
      new PartialFunction[Int, Int] {
        //States that this partial function will take on the task
        def isDefinedAt(x: Int): Boolean = x % 2 == 0

        //What we do if this partial function matches
        def apply(v1: Int): Int = v1 * 2
      }

    val tripleOdds: PartialFunction[Int, Int] = new PartialFunction[Int, Int] {
      def isDefinedAt(x: Int): Boolean = x % 2 != 0

      def apply(v1: Int): Int = v1 * 3
    }

    val composite = doubleEvens orElse tripleOdds
    composite(number)
  }

  def evenOdder2(number: Int):Int = {
    val doubleEvens: PartialFunction[Int, Int] = {
      case x if (x % 2) == 0 => x * 2
    }
    val tripleOdds: PartialFunction[Int, Int] = {
      case x if (x % 2) != 0 => x * 3
    }

    val composite = doubleEvens orElse tripleOdds
    composite(number)
  }
}
