package Scope

object Simple {
  var x = 2 //outer scope
  def double(y:Int):Int = y * 2
  def scopeExample(y: Int):Int = {
    def inner():Int = {
      val x = 3 // inner scope
      double(y) + x //uses inner x
    }
    inner() + x //uses outer x
  }
}
