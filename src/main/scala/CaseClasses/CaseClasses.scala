package CaseClasses

class CaseClasses {
  def printTerm(term: Term):String = {
    term match {
      case Var(n) => n
      case Fun(x, b) =>
        "^" + x + "." + printTerm(b)
      case TermApp(f, v) =>
        print("(") + printTerm(f) + " " + printTerm(v) + ")"
    }
  }
  def isIdentityFun(term: Term): Boolean = term match {
    case Fun(x, Var(y)) if x == y => true
    case _ => false
  }
}
