package InheritanceAndPolymorphism

class Soldier(firstName: String, lastName: String, val rank: String)
  extends Person(firstName, lastName) {

}
