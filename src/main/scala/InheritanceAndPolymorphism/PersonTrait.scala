package InheritanceAndPolymorphism

trait PersonTrait {
  def fullName: String
}
