package InheritanceAndPolymorphism

//Abstract classes are allowed
abstract class Person(val firstName: String, val lastName: String) extends PersonTrait {
  override def fullName: String = firstName + " " + lastName
}
