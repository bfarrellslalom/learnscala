package InheritanceAndPolymorphism

object TypeBounds {
  //Example of how to bound a Type to a class
  def pullRank[A <: Soldier](s1: A, s2: A): A = {
    if(s1.rank < s2.rank) s1 else s2
  }

  def onlyCivs[A >: Engineer <: Civilian](s1: A): String = {
    s1.firstName + " " + s1.lastName
  }
}

