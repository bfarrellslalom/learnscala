package InheritanceAndPolymorphism

class Pilot(firstName: String, lastName: String, rank: String, val squadron: Long)
  extends Soldier(firstName, lastName, rank) {
  override def fullName: String = rank + " " + firstName + " " + lastName + " of the " + squadron
}
