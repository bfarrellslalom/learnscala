package InheritanceAndPolymorphism

class Ranger(firstName: String, lastName: String, rank: String)
  extends Soldier(firstName = firstName, lastName = lastName, rank = rank) {

}
