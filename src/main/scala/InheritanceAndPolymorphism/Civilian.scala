package InheritanceAndPolymorphism

class Civilian(firstName: String, lastName: String)
  extends Person(firstName = firstName, lastName = lastName)  {

}