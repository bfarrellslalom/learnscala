package InheritanceAndPolymorphism

class Engineer(firstName: String, lastName: String)
  extends Civilian(firstName = firstName, lastName = lastName)  {

}
