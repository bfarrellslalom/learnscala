package CodingProblems

import org.scalatest.FunSuite

class TwoSumTest extends FunSuite {
    test("CodingProblems.TwoSum.twoSum") {
      val input = Array(2,7,11,15)
      val inputTarget = 9
      val actual = Array(2, 7)

      val target = new TwoSum()
      val result = target.twoSum(input, inputTarget)

      assert(result sameElements actual)
    }
}
