/*
Given a binary tree, find the lowest common ancestor (LCA) of two given nodes in the tree.

According to the definition of LCA on Wikipedia: “The lowest common ancestor is defined
between two nodes v and w as the lowest node in T that has both v and w as descendants (
where we allow a node to be a descendant of itself).”

Given the following binary search tree:  root = [3,5,1,6,2,0,8,null,null,7,4]
 */
package CodingProblems

import CodingProblems.Helpers.TreeHelpers
import CodingProblems.Helpers.TreeNode
import org.scalatest.FunSuite

class LowestCommonAncestorTest extends FunSuite {
  test("Example1") {
    val nodes = Array(Option(6), Option(2), Option(8), Option(0), Option(4),
      Option(7), Option(9), None, None, Option(3), Option(5))
    val bTree = TreeHelpers.getTreeFromArray(nodes)
    val result = LowestCommonAncestor.lowestCommonAncestor(bTree, 2, 8)
    assert(result.value equals 6)
  }
  test("Example1.Inverse") {
    val nodes = Array(Option(6), Option(2), Option(8), Option(0), Option(4),
      Option(7), Option(9), None, None, Option(3), Option(5))
    val bTree = TreeHelpers.getTreeFromArray(nodes)
    val result = LowestCommonAncestor.lowestCommonAncestor(bTree, 8, 2)
    assert(result.value equals 6)
  }
  test("Example2") {
    val nodes = Array(Option(6), Option(2), Option(8), Option(0), Option(4),
      Option(7), Option(9), None, None, Option(3), Option(5))
    val bTree = TreeHelpers.getTreeFromArray(nodes)
    val result = LowestCommonAncestor.lowestCommonAncestor(bTree, 2, 4)
    assert(result.value equals 2)
  }
  test("Example2.Inverse") {
    val nodes = Array(Option(6), Option(2), Option(8), Option(0), Option(4),
      Option(7), Option(9), None, None, Option(3), Option(5))
    val bTree = TreeHelpers.getTreeFromArray(nodes)
    val result = LowestCommonAncestor.lowestCommonAncestor(bTree, 4, 2)
    assert(result.value equals 2)
  }

  test("Same Node") {
    val nodes = Array(Option(6), Option(2), Option(8), Option(0), Option(4),
      Option(7), Option(9), None, None, Option(3), Option(5))
    val bTree = TreeHelpers.getTreeFromArray(nodes)
    val result = LowestCommonAncestor.lowestCommonAncestor(bTree, 4, 4)
    assert(result.value equals 4)
  }

  test("Root Node") {
    val nodes = Array(Option(6), Option(2), Option(8), Option(0), Option(4),
      Option(7), Option(9), None, None, Option(3), Option(5))
    val bTree = TreeHelpers.getTreeFromArray(nodes)
    val result = LowestCommonAncestor.lowestCommonAncestor(bTree, 6, 6)
    assert(result.value equals 6)
  }


  test("One Node") {
    val nodes = Array(Option(6))
    val bTree = TreeHelpers.getTreeFromArray(nodes)
    val result = LowestCommonAncestor.lowestCommonAncestor(bTree, 6, 6)
    assert(result.value equals 6)
  }

  test("n1 doesn't exists") {
    val nodes = Array(Option(6), Option(2), Option(8), Option(0), Option(4),
      Option(7), Option(9), None, None, Option(3), Option(5))
    val bTree = TreeHelpers.getTreeFromArray(nodes)
    val result = LowestCommonAncestor.lowestCommonAncestor(bTree, 21, 6)
    assert(Option(result).isEmpty)
  }

  test("n2 doesn't exists") {
    val nodes = Array(Option(6), Option(2), Option(8), Option(0), Option(4),
      Option(7), Option(9), None, None, Option(3), Option(5))
    val bTree = TreeHelpers.getTreeFromArray(nodes)
    val result = LowestCommonAncestor.lowestCommonAncestor(bTree, 6, 21)
    assert(Option(result).isEmpty)
  }

  test("nodes is null") {
    val bTree = TreeHelpers.getTreeFromArray[Int](null)
    val result = LowestCommonAncestor.lowestCommonAncestor(bTree, 6, 21)
    assert(Option(result).isEmpty)
  }

  test("nodes is empty") {
    val bTree = TreeHelpers.getTreeFromArray[Int](Array.empty)
    val result = LowestCommonAncestor.lowestCommonAncestor(bTree, 6, 21)
    assert(Option(result).isEmpty)
  }
}
