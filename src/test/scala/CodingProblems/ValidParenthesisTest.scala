package CodingProblems

import org.scalatest.FunSuite

  /*
  Example 1:
Input: "()"
Output: true
Example 2:

Input: "()[]{}"
Output: true
Example 3:

Input: "(]"
Output: false
Example 4:

Input: "([)]"
Output: false
Example 5:

Input: "{[]}"
Output: true

   */
class ValidParenthesisTest extends FunSuite {
    test("CodingProblems.ValidParenthesis.isValid.true") {
      val target = new ValidParenthesis()

      assert(target.isValid("()"))
      assert(target.isValid("()[]{}"))
      assert(target.isValid("([])"))
    }

    test("CodingProblems.ValidParenthesis.isValid.false") {
      val target = new ValidParenthesis()

      assert(!target.isValid("(]"))
      assert(!target.isValid("([)]"))
    }
  }
