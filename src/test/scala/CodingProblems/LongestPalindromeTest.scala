package CodingProblems

import org.scalatest.FunSuite

class LongestPalindromeTest extends FunSuite {
  test("basic") {
    val actual = LongestPalindrome.getLongestPalindrome("aca")
    val expected = "aca"
    assert(actual equals expected)
  }

  test("prefix") {
    val actual = LongestPalindrome.getLongestPalindrome("acab")
    val expected = "aca"
    assert(actual equals expected)
  }

  test("suffix") {
    val actual = LongestPalindrome.getLongestPalindrome("baca")
    val expected = "aca"
    assert(actual equals expected)
  }

  test("middle") {
    val actual = LongestPalindrome.getLongestPalindrome("bacad")
    val expected = "aca"
    assert(actual equals expected)
  }

  test("longestNext") {
    val actual = LongestPalindrome.getLongestPalindrome("bacaeffed")
    val expected = "effe"
    assert(actual equals expected)
  }

  test("longestGap") {
    val actual = LongestPalindrome.getLongestPalindrome("bacagbeffed")
    val expected = "effe"
    assert(actual equals expected)
  }

  test("longestOverlap") {
    val actual = LongestPalindrome.getLongestPalindrome("bacabbad")
    val expected = "bacab"
    assert(actual equals expected)
  }

  test("longestCombined") {
    val actual = LongestPalindrome.getLongestPalindrome("baefgfead")
    val expected = "aefgfea"
    assert(actual equals expected)
  }

  test("Empty") {
    val actual = LongestPalindrome.getLongestPalindrome("")
    val expected = ""
    assert(actual equals expected)
  }

  test("Null") {
    val actual = LongestPalindrome.getLongestPalindrome(null)
    val expected = ""
    assert(actual equals expected)
  }

}
