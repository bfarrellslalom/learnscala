package CodingProblems

import org.scalatest.FunSuite

class FibonacciTest extends FunSuite {
  test("CodingProblems.Fibonacci.fibTail") {
    val target = new Fibonacci()
    assert(target.fibTail(5) === 5)
  }
}
