package CodingProblems

import org.scalatest.FunSuite

class ValidPalindromeTest extends FunSuite {
  test("CodingProblems.ValidPalindrome.isPalindrome.true") {
    val target = new ValidPalindrome()

    assert(target.isPalindrome("imi"))
    assert(target.isPalindrome("im i"))
    assert(target.isPalindrome("imI"))
  }

  test("CodingProblems.ValidPalindrome.isPalindrome.false") {
    val target = new ValidPalindrome()

    //noinspection SpellCheckingInspection
    assert(!target.isPalindrome("imia"))
    assert(!target.isPalindrome("im ia"))
  }
}
