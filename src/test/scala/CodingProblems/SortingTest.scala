package CodingProblems

import org.scalatest.FunSuite

class SortingTest extends FunSuite {
  test("SortingTest") {
    val target = new Sorting()
    val input = List(4, 2, 7, 3, 9, 8, 1, 6, 5)
    val result = List(1, 2, 3, 4, 5, 6, 7, 8, 9)

    assert(target.mergeSort(input) equals result)
  }
}
