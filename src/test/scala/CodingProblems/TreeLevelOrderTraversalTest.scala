package CodingProblems

import org.scalatest.FunSuite
import CodingProblems.Helpers.TreeHelpers

class TreeLevelOrderTraversalTest extends FunSuite {
  test("Example") {
    val input = TreeHelpers.getTreeFromArray(Array(Option(3), Option(9), Option(20), None, None, Option(15), Option(7)))
    val result = TreeLevelOrderTraversal.levelOrder(input)
    val expected = List(List(3), List(9, 20), List(15, 7))

    assert(result equals expected.flatten)
  }

  test("LargeExample") {
    val input = TreeHelpers.getTreeFromArray(Array(Option(3), Option(9), Option(20), None, None, Option(15), Option(7),
      None, None, None, None, Option(12), Option(17), None, None))
    val result = TreeLevelOrderTraversal.levelOrder(input)
    val expected = List(List(3), List(9, 20), List(15, 7), List(12, 17))

    assert(result equals expected.flatten)
  }

  test("LargerExample") {
    val input = TreeHelpers.getTreeFromArray(Array(Option(1), Option(2), Option(3), Option(4), Option(5), Option(6),
      Option(7), Option(8), Option(9), Option(10), Option(11), Option(12), Option(13), Option(14), Option(15)))
    val result = TreeLevelOrderTraversal.levelOrder(input)
    val expected = List(List(1), List(2, 3), List(4, 5, 6, 7), List(8, 9, 10, 11, 12, 13, 14, 15))

    assert(result equals expected.flatten)
  }

  test("One Node") {
    val input = TreeHelpers.getTreeFromArray(Array(Option(5)))
    val result = TreeLevelOrderTraversal.levelOrder(input)
    val expected = List(List(5))

    assert(result equals expected.flatten)
  }

  test("One Node - None") {
    val input = TreeHelpers.getTreeFromArray[Int](Array(None))
    val result = TreeLevelOrderTraversal.levelOrder(input)
    assert(result.isEmpty)
  }

  test("Null") {
    val result = TreeLevelOrderTraversal.levelOrder(null)
    assert(result.isEmpty)
  }
}
