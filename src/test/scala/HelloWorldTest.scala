import org.scalatest.FunSuite

class HelloWorldTest extends FunSuite {
  test("HelloWorld.Hello") {
    val target = new HelloWorld()
    assert(target.SayHello("Brian") === "Hello Brian")
  }

}
