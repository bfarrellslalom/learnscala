package Comparison

import org.scalatest.FunSuite

//noinspection OptionEqualsSome
class SomeNoneTest extends FunSuite{
  test("Comparison.SomeNone.getNumber") {
    val target = new SomeNone()
    assert(target.getNumber == Some(3))
    assert(target.getNumber.map(_ * 1.5) == Some(4.5))
    assert(target.getNumber.fold(1)(_ * 3) == 9)
  }

  test("Comparison.SomeNone.getNoNumber") {
    val target = new SomeNone()
    assert(target.getNoNumber.isEmpty)
    assert(target.getNoNumber.map(_ * 1.5).isEmpty)
    assert(target.getNoNumber.fold(5)(_ * 3) == 5)
  }
}
