package Comparison

import org.scalatest.FunSuite

class ByNameTest extends FunSuite{
  test("Comparison.ByName.calc") {
    val target = new ByName()
    val result = target.calc(() => 14 + 15)
    assert(result equals Right(29)) //TODO: Look into right/left
  }

  test("Comparison.ByName.calc2") {
    val target = new ByName()
    val result = target.calc2{14 + 15}
    assert(result equals Right(29))
  }

  test("Comparison.ByName.PigLatinizer") {
    val result = (new ByName).PigLatinizer {
      val x = "foo"
      val y = "bar"
      x ++ y // returns foobar
    }

    assert(result equals "oobarfay")
  }
}
