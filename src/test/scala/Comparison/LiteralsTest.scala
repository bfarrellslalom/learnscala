package Comparison

import org.scalatest.FunSuite

class LiteralsTest extends FunSuite{
  test("Numbers") {
    assert(2 equals 2)
    assert(0x30F equals 783)
    assert(-2 equals -2)
    assert(-0x30F equals -783)

    val n1Long: Long = 2
    val n2Long: Long = 0x30F
    assert(n1Long equals 2L)
    assert(n2Long equals 783L)

    val n1Float:Float = 2.1F
    val n2Float:Float = 0x30F
    val n1Double = 2.1
    val n2Double:Double = 0x30F
    val n3Double:Double = 9.20E-9D

    assert(n1Float equals 2.1f)
    assert(n2Float equals 783.0f)
    assert(n1Double equals 2.1D)
    assert(n2Double equals 783D)
    assert(n3Double equals 9.20E-9D)
    assert("%.10f".format(n3Double) equals "0.0000000092")
  }

  test("StringsAndChars") {
    assert('a' !== "a")
    assert('a'.toString === "a")
    assert('\141'.toString === "a") //141 = a in unicode
    assert('\"'.toString === "\"") //escape for "
    assert('\\'.toString === "\\") //escape for \
  }
}
