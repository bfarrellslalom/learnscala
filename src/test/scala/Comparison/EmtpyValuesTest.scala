package Comparison

import org.scalatest.FunSuite

import scala.xml.Null

//noinspection ComparingUnrelatedTypes,EmptyCheck,TypeAnnotation
class EmtpyValuesTest extends FunSuite {
  test("Null Comparisons") {
    //can't use "equals" as it isn't a class
    assert(null != Null)
    assert(null != Unit)
    assert( null != Nil)
    assert(!Null.equals(Unit))
    assert(!Null.equals(Nil))
    assert(Null equals Null)
  }

  test("Unit Comparison") {
    def method1() = ()
    assert(Unit equals Unit)
    assert(method1().isInstanceOf[Unit]) //Unit is similiar to void in Java
  }

  test("Nil Comparison") {
    assert(Nil equals Nil)
    assert(Nil equals List()) // same as empty list
    assert(Nil equals None.toList)
  }

  test("None comparison") {
    assert(None equals None)
    assert(None.toString equals "None")
    assert(None.isEmpty)
    assert(None.asInstanceOf[Any] equals None)
    assert(None.asInstanceOf[AnyRef] equals None)
    assert(None.asInstanceOf[AnyVal] equals None)
  }

  test("Nothing Comparison") {
    assert(List().isInstanceOf[List[Nothing]])
    assert(List[Nothing]() equals List()) //requires () on initial list
  }

  test("Otion comparison") {
    val targetNone: Option[String] = None
    val targetSome: Option[String] = Some("Some")
    val targetNull: Option[String] = null //Don't do this
    val targetNull2: Option[String] = Option(null) //Do this


    assert(targetNone.isEmpty)
    assert(targetNone equals None)
    assert(targetSome.isDefined)
    assert(targetSome.nonEmpty)
    assert(targetNull == null) //anti patter on checking nulls
    assert(targetNull2.isEmpty) //better practice

    assert(targetNone.getOrElse("Else") equals "Else")
    assert(targetSome.getOrElse("Else") equals "Some")
  }
}
