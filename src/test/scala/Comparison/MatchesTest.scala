package Comparison

import org.scalatest.FunSuite

class MatchesTest extends FunSuite{
  test("Comparison.MatchesTest.simpleMatch") {
    val target = new Matches()
    assert(target.simpleMatch(1) equals "One")
    assert(target.simpleMatch(3) equals "Many")
  }

  test("Comparison.MatchesTest.patternMatch") {
    val target = new Matches()
    assert(target.patternMatch equals 2)
  }

  test("Comparison.MatchesTest.complexResult") {
    val target = new Matches()
    assert(target.complexResult("red") equals (255, 0, 0))
  }

  test("Comparison.MatchesTest.complexMatch") {
    val target = new Matches()
    assert(target.complexMatch("Value1", "Value2") equals "Found")
    assert(target.complexMatch("Value1", "Nope") equals "Not Found")
  }

  test("Comparison.MatchesTest.wildCard") {
    val target = new Matches()
    assert(target.wildCard("Value1", "Value2") equals "Found")
    assert(target.wildCard("Value3", "Nope") equals "Found")
  }

  test("Comparison.MatchesTest.substitute") {
    val target = new Matches()
    assert(target.substitute("Value1", "Value2") equals "Found")
    assert(target.substitute("Value3", "Nope") equals "Nope")
  }

  test("Comparison.MatchesTest.backquote") {
    val target = new Matches()
    assert(!target.backquote(1, 2))
    assert(target.backquote(1, 1))
  }

  test("Comparison.MatchesTest.list") {
    val target = new Matches()
    assert(target.list(List(1, 2, 3)) equals 2)
    assert(target.list2(List(1)) equals 0)
  }

  test("Comparison.MatchesTest.list2") {
    val target = new Matches()
    assert(target.list2(List(1, 2, 3)) equals 2)
    assert(target.list2(List(1)) equals 0)
  }

  test("Comparison.MatchesTest.list3") {
    val target = new Matches()
    assert(target.list3(List(1, 2)) equals 1)
    assert(target.list3(List(1, 2, 3)) equals 0)
    assert(target.list3(List(1)) equals 0)
  }

  test("Comparison.MatchesTest.list4") {
    val target = new Matches()
    assert(target.list4(List(1, 2, 3)) equals Nil)
    assert(target.list4(List(1, 2, 3, 4)) equals List(4))
    assert(target.list4(List(1, 2, 3, 4, 5)) equals List(4, 5))
    assert(target.list4(List(1)) equals 0)
  }
}
