import org.scalatest.FunSuite

package Extractors {
  class MethodsTest extends FunSuite {
    test("Methods.myFamily") {
      val family = Person("Bob", "FamilyName")
      val friend = Person("Bob", "FriendLastName")

      assert(Methods.myFamily(family) equals "Hello Bob")
      assert(Methods.myFamily(friend) equals "Hello Friend")
    }

    test("Methods.chopShop") {
      val ChopShop(a, b, c, d) = Car("Chevy", "Camaro", 1978, 120)
      assert(a equals "Chevy")
      assert(b equals "Camaro")
      assert(c equals 1978.toShort)
      assert(d equals 120.toShort)
    }

    test("Methods.getUserDesc") {
      val freeUser1: User = new FreeUser("Daniel", 3000, 0.7d)
      val freeUser2: User = new FreeUser("Bob", 5000, 0.9d)
      val premiumUser: User = new PremiumUser("Joe", 45)
      val defaultUser: User = new DefaultUser("Nate", 1200)

      assert(Methods.getUserDesc(freeUser1) equals "Hello Daniel")
      assert(Methods.getUserDesc(freeUser2) equals "Bob, what can we do for you today?")
      assert(Methods.getUserDesc(premiumUser) equals "Welcome back, dear Joe")
      assert(Methods.getUserDesc(defaultUser) equals "Hello, Nate")
    }
  }
}