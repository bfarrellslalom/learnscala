package Extractors

import org.scalatest.FunSuite

class HelloTest extends FunSuite{
  test("Hello.TwiceOnce") {
    val first = Hello("myname")
    //the match case causes unapply to be called
    val second = "myname" match {
      case Hello(n) => n
    }
    val third = Hello("myname") //no case so unapply wasn't called

    assert(first equals "Hello myname")
    assert(second equals "Hello again, myname")
    assert(third equals "Hello myname")
  }

  test("Hello.TwiceOnceExplicit") {
    val first = Hello.apply("myname")
    val second = Hello.unapply("myname").get

    assert(first equals "Hello myname")
    assert(second equals "Hello again, myname")
  }
}
