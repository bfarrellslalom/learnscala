import org.scalatest.FunSuite

class AppMainTest extends FunSuite {
  test("AppMain.Hello") {
    val target = new HelloWorld()
    assert(target.SayHello("Brian") === "Hello Brian")
  }
}
