package Traits

import org.scalatest.FunSuite

class TraitsTest extends FunSuite{
  test("Traits.getComposite1") {
    val target = TraitComposite.getComposite1
    val actual = target.aId + target.bId

    assert(actual equals 3)
  }

  test("Traits.getComposite") {
    val target = TraitComposite.getComposite2
    val actual = target.requireId + target.bId

    assert(actual equals 12)
  }
}
