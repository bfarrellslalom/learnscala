package Functions

import org.scalatest.FunSuite

class InFixNotesTest extends FunSuite {
  test("Functions.InFixNotes.unary") {
    val target = new InFixNotes()
    assert(target.unary_+ equals "on")
    assert(target.unary_- equals "off")
    assert(+target equals "on")
    assert(-target equals "off")
  }

  test("Functions.InFixNotes.announceCouple") {
    val target = new InFixNotes()

    val p1 = target.Person("romeo")
    val p2 = target.Person("juliet")
    val couple1 = new target.Loves(p1, p2)
    val couple2 = p1 loves p2

    assert(target.announceCouple(couple1) equals "romeo is in love with juliet")
    assert(target.announceCouple(couple2) equals "romeo is in love with juliet")
  }
}
