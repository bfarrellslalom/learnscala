package Functions

import org.scalatest.FunSuite

class LambdaFnTest extends FunSuite {
  test("Functions.LambaFn.Lambda1") {
    val target = new LambaFn()
    assert(target.lambda1(1, 2) == 3)
  }
  test("Functions.LambaFn.Lambda2") {
    val target = new LambaFn()
    assert(target.lambda2(1) == 2)
  }
  test("Functions.LambaFn.Lambda3") {
    val target = new LambaFn()
    assert(target.lambda3(1, 2) == 3)
  }
  test("Functions.LambaFn.Lambda4") {
    val target = new LambaFn()
    assert(target.lambda4(1, 2) == 3)
  }
  test("Functions.LambaFn.Lambda5") {
    val target = new LambaFn()
    assert(target.lambda5(1, 2) == 3)
  }
}
