package Functions

import org.scalatest.FunSuite

class HighOrderFnTest extends FunSuite {
  test("Functions.HighOrderFn.makeUpper") {
    val target = new HighOrderFn()
    assert(target.makeUpper(List("abc", "xyz", "123")) == List("ABC", "XYZ", "123"))
  }

  test("Functions.HighOrderFn.makeWhatEverYouLike") {
    val target = new HighOrderFn()
    assert(target.makeWhatEverYouLike(List("ABC", "XYZ", "123"), _.toLowerCase) == List("abc", "xyz", "123"))
  }
}
