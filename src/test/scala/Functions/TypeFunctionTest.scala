package Functions

import org.scalatest.FunSuite

class TypeFunctionTest extends FunSuite{
  test("divide") {
    assert(TypeFunction.divide(6, 4) equals Right((1, 2)))
    assert(TypeFunction.divide(2, 0) equals Left("Division by zero"))
    assert(TypeFunction.divide(8, 4) equals Right(2, 0))

  }
}
