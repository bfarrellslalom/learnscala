package Functions

import org.scalatest.FunSuite

class TraitFunctionTest extends FunSuite {
  test("TraitFunction"){
    class multi extends TraitFunction[Int, Int] {
      override def apply(x: Int): Int = x * x
    }

    val m = new multi
    //Note that apply is a special method
    assert(m.apply(5) equals 25)

    //Note - In this case, skipping to the simple example is better syntax
  }

  test("simple") {
    val f = (x: Int) => x * x
    assert(f(5) equals 25)
  }
}
