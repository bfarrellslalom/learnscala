package Functions

import org.scalatest.FunSuite

class CurryingTest extends FunSuite {
  test("Functions.Currying.add") {
    val target = new Currying()
    assert(target.add(1)(2) == 3)
  }

  test("Functions.Currying.f") {
    val target = new Currying()
    assert(target.f(1, 2) == 3)
  }

  test("Functions.Currying.g") {
    val target = new Currying()
    assert(target.g(1)(2) == 3)
  }

  test("Functions.Currying.curry") {
    val target = new Currying()
    assert(target.curry(target.f)(1)(2) == 3)
  }

  test("Functions.Currying.myCurry") {
    val target = new Currying()
    assert(target.myCurry(3)(4) equals 7)
  }

  test("Functions.Currying.hello") {
    val target = new Currying()
    assert(target.hello("sup")("brian") equals "sup, brian")
  }
}
