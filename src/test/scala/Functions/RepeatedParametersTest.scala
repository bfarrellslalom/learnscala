package Functions

import org.scalatest.FunSuite

class RepeatedParametersTest extends FunSuite{
  test("RepeatedParameters.repeatedParameterMethod") {
    val num = 3
    val food = "egg"
    val items = List("a delicious sandwich", "protein", "high cholesterol")
    val expectedSimple = "3 eggs can give you a delicious sandwich, protein, high cholesterol"
    val expectedList = "3 eggs can give you List(a delicious sandwich, protein, high cholesterol)"

    val result1 = RepeatedParameters.repeatedParameterMethod(num, food, items.head, items(1), items(2))
    assert(result1 equals expectedSimple)

    val result2 = RepeatedParameters.repeatedParameterMethod(num, food, items)
    assert(result2 equals expectedList)

    //_* is a special param for varargs, and can be used to replace an arg of Array[Any]
    val result3 = RepeatedParameters.repeatedParameterMethod(num, food, items: _*)
    assert(result3 equals expectedSimple)
  }

  test("helloPeople") {
    val names = List("Brian", "Nate", "Doug")
    assert(RepeatedParameters.helloPeople(names: _*) equals "Hello Brian, Nate, Doug!")
    assert(RepeatedParameters.helloPeople("Brian") equals "Hello Brian!")

  }
}
