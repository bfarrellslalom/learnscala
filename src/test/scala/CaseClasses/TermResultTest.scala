package CaseClasses

import org.scalatest.FunSuite

class TermResultTest extends FunSuite{
  test("printTerm") {
    val term = Fun("1", Var("2"))
    val target = new CaseClasses()
    val result = target.printTerm(term)

    assert(result equals "^1.2")
  }
}
