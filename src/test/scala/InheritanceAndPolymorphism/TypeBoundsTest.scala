package InheritanceAndPolymorphism

import org.scalatest.FunSuite

/*
For more information see this lesson.
 */
class TypeBoundsTest extends FunSuite {
  test("pullRank") {
    val s1 = new Pilot("John", "Doe", "Captian", 21)
    val s2 = new Marine("Mike", "Doe", "Private")

    assert(TypeBounds.pullRank[Soldier](s1, s2) equals s1)
    //assert(TypeBounds.pullRank[Person](s1, s2) equals s1) //won't compile
    //var civ = new Civilian("Jane", "Doe")
    //assert(TypeBounds.pullRank[Person](civ, s1) equals s1) // won't compile
  }

  test("noPilots") {
    val civ = new Civilian("John", "Doe")
    assert(TypeBounds.onlyCivs(civ) equals civ.firstName + " " + civ.lastName)


    //Won't work if civ is cast to person
    //val civ2:Person = civ
    //assert(TypeBounds.onlyCivs(civ2) equals civ2.firstName + " " + civ2.lastName)

    //Won't work
    //val soldier:Person = new Marine("Mike", "Doe", "Private")
    //assert(TypeBounds.onlyCivs(soldier) equals soldier.firstName + " " + soldier.lastName)
  }
}
