package InheritanceAndPolymorphism

import org.scalatest.FunSuite

class SoldierTest extends FunSuite{
  test("Soldier.class") {
    val s = new Soldier("John", "Doe", "Captain")
    assert(s.isInstanceOf[PersonTrait])
    assert(s.isInstanceOf[Person])
    assert(!s.isInstanceOf[Pilot])

    assert(s.fullName equals "John Doe")
  }

  test("Polymorphism.PilotToPeron") {
    val pilot = new Pilot("John", "Doe", "Captain", squadron = 23)
    val person: Person = pilot

    assert(pilot.fullName equals "Captain John Doe of the 23")
    assert(person.fullName equals pilot.fullName)
  }

  test("Polymorphism.GetClassNames") {
    val pilot = new Pilot("John", "Doe", "Captain", squadron = 23)
    val person: Person = pilot
    val container = new InvariantContainer(person)

    assert(classOf[Pilot].getSimpleName equals "Pilot")
    assert(classOf[Pilot].getCanonicalName equals "InheritanceAndPolymorphism.Pilot")
    assert(person.getClass.getSimpleName equals "Pilot")
    assert(person.getClass.getCanonicalName equals "InheritanceAndPolymorphism.Pilot")
    assert(container.contents equals "Person")
  }

  test("Polymorphism.Variance") {
    val soldier:Soldier = new Pilot("John", "Doe", "Captain", squadron = 23)
    val covarience1:CovarientContainer[Person] = new CovarientContainer(soldier)
    val covarience2:CovarientContainer[Person] = new CovarientContainer[Person](soldier)
    //val covarience3:CovarientContainer[Pilot] = new CovarientContainer(soldier) //BAD!
    val contravarience1:ContravarientContainer[Pilot] = new ContravarientContainer(soldier)
    val contravarience2:ContravarientContainer[Person] = new ContravarientContainer(soldier)

    assert(covarience1.contents equals "Soldier")
    assert(covarience2.contents equals "Person")
    assert(contravarience1.contents equals "Soldier")
    assert(contravarience2.contents equals "Person")
  }

  //Invariant
  class InvariantContainer[A](val a: A)(implicit manifest: scala.reflect.Manifest[A]) {
    def contents: String = manifest.runtimeClass.getSimpleName
  }

  //Covariant [+A]
  class CovarientContainer[+A](val a: A)(implicit manifest: scala.reflect.Manifest[A]) {
    def contents: String = manifest.runtimeClass.getSimpleName
  }

  //Contravariant [-A] (can no longer use val)
  class ContravarientContainer[-A](a: A)(implicit manifest: scala.reflect.Manifest[A]) {
    def contents: String = manifest.runtimeClass.getSimpleName
  }
}
