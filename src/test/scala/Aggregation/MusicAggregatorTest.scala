package Aggregation

import org.scalatest.FunSuite

class MusicAggregatorTest extends FunSuite{
  test("symbolAggregation") {
    val note1:Symbol = new NoteA(Quarter, 3)
    val note2:Symbol = new NoteB(Half, 3)
    val rest:Symbol = Rest(Whole)

    val col = List(note1, note2, rest)

    val result = col.foldLeft(0f)(_ + MusicAggregator.getDuration(_))
    assert(result equals 1.75f)
  }
}
