import org.scalatest.FunSuite

package LearnImplicit {
  class LearnImplicitTest extends FunSuite {
    test("LearnImplicit.Wrapper.sum") {
      import Wrapper._ //Note - Need to import implicits for them to be available in a different class

      assert(Wrapper.sum(List(1, 2, 3)) equals 6)
      assert(Wrapper.sum(List("A", "B", "C")) equals "ABC")
    }

    test("LearnImplicit.Wrapper.isOdd") {
      import Wrapper._ //Note - Need to import implicits for them to be available in a different class

      assert(!20.isOdd)
      assert(19.isOdd)
    }

    test("LearnImplicit.Wrapper.split") {
      import Wrapper._ //Note - Need to import implicits for them to be available in a different class

      assert(123.split equals List(1, 2, 3))
    }
  }

}