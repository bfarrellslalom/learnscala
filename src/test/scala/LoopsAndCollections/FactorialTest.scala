package LoopsAndCollections

import org.scalatest.FunSuite

class FactorialTest extends FunSuite{
  test("factorial") {
    assert(Factorial.factorial(3) equals 6)
    assert(Factorial.factorial(4) equals 24)
  }
}
