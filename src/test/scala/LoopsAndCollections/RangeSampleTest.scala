package LoopsAndCollections

import org.scalatest.FunSuite

class RangeSampleTest extends FunSuite {
  test("LoopsAndCollections.RangeSample.getRange") {
    val target = new RangeSample()
    assert(target.getRange.size equals 10)
    assert(target.getRange(1) equals 1)
    assert(target.getRange.head equals 0)
    assert(target.getRange.last equals 9)
    assert(target.getRange.tail equals Range(1, 10))
    assert(target.getRange equals (0 until 10))
    assert(target.getRange equals (0 to 9))
  }

  test("LoopsAndCollections.RangeSample.getRangeInc") {
    val target = new RangeSample()
    assert(target.getRangeInc.size equals 3)
    assert(target.getRangeInc(1) equals 5)
    assert(target.getRangeInc.head equals 2)
    assert(target.getRangeInc.last equals 8)
  }

  test("rangeLoop") {
    val target = new RangeSample()
    val result = target.rangeLoop
    assert(result equals List(2, 5, 8))
  }
}
