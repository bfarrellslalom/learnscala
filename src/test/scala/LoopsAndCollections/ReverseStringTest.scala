package LoopsAndCollections

import org.scalatest.FunSuite

//noinspection SpellCheckingInspection
class ReverseStringTest extends FunSuite {
  test("LoopsAndSequences.ReverseString.reverseString") {
    val input = "test"
    val actual = "tset"

    val target = new ReverseString()
    val result = target.reverseString(input)

    assert(result === actual)
  }
}
