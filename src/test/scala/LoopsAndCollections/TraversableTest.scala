package LoopsAndCollections

import org.scalatest.FunSuite

//noinspection SimplifiableFoldOrReduce
class TraversableTest extends FunSuite{
  test("Append") {
    val set = Set(1, 9, 10, 22)
    val list = List(3, 4, 5, 10)
    val result = set ++ list
    assert(result equals Set(1, 3, 4, 5, 9, 10, 22))

    val result2 = list ++ set
    assert(result2 equals List(3, 4, 5, 10, 1, 9, 10, 22))
  }

  test("map") {
    val set = Set(1, 3, 4, 6)
    val result = set.map(_ * 4)
    assert(result equals Set(4, 12, 16, 24))
  }

  test("flatten") {
    val list = List(List(1), List(2, 3, 4), List(5, 6, 7), List(8, 9, 10))
    assert(list.flatten equals List(1, 2, 3, 4, 5, 6, 7, 8, 9, 10))
  }

  test("flatMap") {
    val list = List(List(1), List(2, 3, 4), List(5, 6, 7), List(8, 9, 10))
    val result = list.flatMap(_.map(_ * 4))
    assert(result equals List(4, 8, 12, 16, 20, 24, 28, 32, 36, 40))
  }

  test("flatMapFancy") {
    val list = List(List(1), List(2, 3, 4), List(5, 6, 7), List(8, 9, 10))
    val result = list.flatten.flatMap(it => if (it % 2 == 0) Some(it) else None)
    assert(result equals List(2, 4, 6, 8 ,10))

    val list2 = List(1, 2, 3, 4, 5)
    val result2 = list2.flatMap(it ⇒ if (it % 2 == 0) Some(it) else None)
    assert(result2 equals List(2, 4))
  }

  test("collect") {
    val list = List(4, 6, 7, 8, 9, 13, 14)
    val result = list.collect {
      case x: Int if x % 2 == 0 ⇒ x * 3
    }

    assert(result equals List(12, 18, 24, 42))
  }

  test("collectionChain") {
    val list = List(4, 6, 7, 8, 9, 13, 14)
    val partialFunction1: PartialFunction[Int, Int] = {
      case x: Int if x % 2 == 0 ⇒ x * 3
    }
    val partialFunction2: PartialFunction[Int, Int] = {
      case y: Int if y % 2 != 0 ⇒ y * 4
    }
    val result = list.collect(partialFunction1 orElse partialFunction2)

    assert(result equals List(12, 18, 28, 24, 36, 52, 42))
  }

  test("toStream") {
    val list = List(4, 6, 7, 8, 9, 13, 14)
    val result = list.toStream
    assert(result.isInstanceOf[Stream[_]])
    assert(result take 3 equals Stream(4, 6, 7))
  }

  //TODO: This is interesting, come back and study cons and streams more.
  test("hasDefiniteSize") {
    val map = Map("Phoenix" → "Arizona", "Austin" → "Texas")
    assert(map.hasDefiniteSize)

    val stream = Stream.cons(0, Stream.cons(1, Stream.empty))
    assert(!stream.hasDefiniteSize)
  }

  test("filter") {
    val array = Array(87, 44, 5, 4, 200, 10, 39, 100)
    assert(array.filter(_ < 100) sameElements Array(87, 44, 5, 4, 10, 39))
  }

  test("split") {
    val array = Array(87, 44, 5, 4, 200, 10, 39, 100)
    val result = array splitAt 3
    assert(result._1 sameElements Array(87, 44, 5))
    assert(result._2 sameElements Array(4, 200, 10, 39, 100))
  }

  test("span") {
    val array = Array(87, 44, 5, 4, 200, 10, 39, 100)
    val result = array.span(_ < 100)
    assert(result._1 sameElements  Array(87, 44, 5, 4))
    assert(result._2 sameElements  Array(200, 10, 39, 100))
  }

  test("partition") {
    val array = Array(87, 44, 5, 4, 200, 10, 39, 100)
    val result = array partition (_ < 100)
    assert(result._1 sameElements Array(87, 44, 5, 4, 10, 39))
    assert(result._2 sameElements Array(200, 100))
  }

  //TODO: study more, but this is amazing
  test("groupBy") {
    val array = Array(87, 44, 5, 4, 200, 10, 39, 100)

    val oddAndSmallPartial: PartialFunction[Int, String] = {
      case x: Int if x % 2 != 0 && x < 100 ⇒ "Odd and less than 100"
    }

    val evenAndSmallPartial: PartialFunction[Int, String] = {
      case x: Int if x != 0 && x % 2 == 0 && x < 100 ⇒ "Even and less than 100"
    }

    val negativePartial: PartialFunction[Int, String] = {
      case x: Int if x < 0 ⇒ "Negative Number"
    }

    val largePartial: PartialFunction[Int, String] = {
      case x: Int if x > 99 ⇒ "Large Number"
    }

    val zeroPartial: PartialFunction[Int, String] = {
      case x: Int if x == 0 ⇒ "Zero"
    }

    val result = array groupBy {
      oddAndSmallPartial orElse
        evenAndSmallPartial orElse
        negativePartial orElse
        largePartial orElse
        zeroPartial
    }

    assert(result("Even and less than 100").length equals 3)
    assert(result("Large Number").length equals 2)
  }

  test("foldLeft") {
    val list = List(5, 4, 3, 2, 1)
    val result = (0 /: list) { (`running total`, `next element`) ⇒
      `running total` - `next element`
    }
    assert(result equals -15)

    val result2 = list.foldLeft(0) { (`running total`, `next element`) ⇒
      `running total` - `next element`
    }
    assert(result2 equals -15)

    val result3 = (0 /: list)(_ - _) //Short hand
    assert(result3 equals -15)

    val result4 = list.foldLeft(0)(_ - _)
    assert(result4 equals -15)
  }

  test("foldRight") {
    val list = List(5, 4, 3, 2, 1)
    val result = (list :\ 0) { (`running total`, `next element`) ⇒
      `running total` - `next element`
    }
    assert(result equals 3)

    val result2 = list.foldRight(0) { (`running total`, `next element`) ⇒
      `running total` - `next element`
    }
    assert(result2 equals 3)

    val result3 = (list :\ 0)(_ - _) //Short hand
    assert(result3 equals 3)

    val result4 = list.foldRight(0)(_ - _)
    assert(result4 equals 3)
  }

  test("reduceLeft") {
    val intList = List(5, 4, 3, 2, 1)
    assert(intList.reduceLeft { _ + _ } equals 15)

    val stringList = List("Do", "Re", "Me", "Fa", "So", "La", "Te", "Do")
    assert(stringList.reduceLeft { _ + _ } equals "DoReMeFaSoLaTeDo")
  }

  test("reduceRight") {
    val intList = List(5, 4, 3, 2, 1)
    assert(intList.reduceRight { _ + _ } equals 15)

    val stringList = List("Do", "Re", "Me", "Fa", "So", "La", "Te", "Do")
    assert(stringList.reduceRight { _ + _ } equals "DoReMeFaSoLaTeDo")
  }

  //Important - reduceRight tail recursion
  /*
  The naive recursive implementation of reduceRight is not tail recursive and would lead to a stack overflow
  if used on larger traversables. However, reduceLeft can be implemented with tail recursion.

  Note the reverse and the flipping of the variables.
   */
  test("reduceRightTail") {
    val intList = List(5, 4, 3, 2, 1)
    assert(intList.reduceRight((x, y) => x - y) equals 3)
    assert(intList.reverse.reduceLeft((x, y) => y - x) equals 3)
    assert(intList.reverse.reduce((x, y) => y - x) equals 3)
  }

  test("transpose") {
    val list = List(List(1, 2, 3), List(4, 5, 6), List(7, 8, 9))
    assert(list.transpose equals List(List(1, 4, 7), List(2, 5, 8), List(3, 6, 9)))
    val list2 = List(List(1), List(4))
    assert(list2.transpose equals List(List(1, 4)))

  }
}
