package LoopsAndCollections

import org.scalatest.FunSuite

import scala.collection.mutable

class CollectionOperatorsTest extends FunSuite {
  test("LoopsAndCollections.CollectionOperators.addNewAndExisting") {
    val target = new CollectionOperators()
    val input = mutable.MutableList(1, 2)
    val result = target.addNewAndExisting(input, 3)
    assert(result equals mutable.MutableList(1, 2, 3))
    assert(input.length === 3)
  }
  test("LoopsAndCollections.CollectionOperators.addNewOnly") {
    val target = new CollectionOperators()
    val input = mutable.MutableList(1, 2)
    val result = target.addNewOnly(input, 3)
    assert(result equals mutable.MutableList(1, 2, 3))
    assert(input.length === 2)
  }

  test("LoopsAndCollections.CollectionOperators.union1") {
    val target = new CollectionOperators()
    val result = target.union1(Set(1, 2, 3), Set(3, 4, 5))
    assert(result equals Set(1, 2, 3, 4, 5))
  }
  test("LoopsAndCollections.CollectionOperators.union2") {
    val target = new CollectionOperators()
    val result = target.union2(Set(1, 2, 3), Set(3, 4, 5))
    assert(result equals Set(1, 2, 3, 4, 5))
  }

  test("LoopsAndCollections.CollectionOperators.intersect") {
    val target = new CollectionOperators()
    val result = target.intersect(Set(1,2, 3, 4), Set(3, 4, 5, 6))
    assert(result equals Set(3, 4))
  }

  test("LoopsAndCollections.CollectionOperators.diff1") {
    val target = new CollectionOperators()
    val result = target.diff1(Set(1,2, 3, 4), Set(3, 4, 5, 6))
    assert(result equals Set(1, 2)) //only returns diff from first set, not both
  }
  test("LoopsAndCollections.CollectionOperators.diff2") {
    val target = new CollectionOperators()
    val result = target.diff2(Set(1,2, 3, 4), Set(3, 4, 5, 6))
    assert(result equals Set(1, 2)) //only returns diff from first set, not both
  }
}
