package LoopsAndCollections

import org.scalatest.FunSuite

class SequencesTest extends FunSuite {
  test("LoopsAndSequences.Sequences.reverseMap") {
    val target = new Sequences()
    assert(target.reverseMap() equals List("olleh", "dlrow"))
  }
}
