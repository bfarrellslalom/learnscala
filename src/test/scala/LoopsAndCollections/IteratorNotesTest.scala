package LoopsAndCollections

import org.scalatest.FunSuite

//noinspection SameElementsToEquals,CorrespondsUnsorted
class IteratorNotesTest extends FunSuite{
  test("LoopsAndCollections.IteratorNotes.getNext") {
    val target = new IteratorNotes()

    assert(target.getNext(List(1, 2)) equals 1)
  }

  test("LoopsAndCollections.IteratorNotes.getGrouped") {
    val target = new IteratorNotes()
    val i = target.getGrouped(List(1, 2, 3, 4, 5 ,6))
    assert(i.next() equals List(1, 2, 3))
    assert(i.next() equals List(4, 5, 6))
  }

  test("LoopsAndCollections.IteratorNotes.getSliding") {
    val target = new IteratorNotes()
    val i = target.getSliding(List(1, 2, 3, 4, 5 ,6))
    assert(i.next() equals List(1, 2, 3))
    assert(i.next() equals List(2, 3, 4))
  }

  test("LoopsAndCollections.IteratorNotes.getSlidingStep") {
    val target = new IteratorNotes()
    val i = target.getSlidingStep(List(1, 2, 3, 4, 5 ,6, 7, 8, 9))
    assert(i.next() equals List(1, 2, 3))
    assert(i.next() equals List(4, 5, 6))
    assert(i.next() equals List(7, 8, 9))
  }

  test("LoopsAndCollections.IteratorNotes.getZip") {
    val target = new IteratorNotes()
    assert(target.getZip(List(1, 2, 3), List("One", "Two", "Three")) equals List((1, "One"), (2, "Two"), (3, "Three")))
    assert(target.getZip(List(1, 2, 3), List("One", "Two")) equals List((1, "One"), (2, "Two")))
  }

  test("LoopsAndCollections.IteratorNotes.getZipAll") {
    val target = new IteratorNotes()
    assert(target.getZipAll(List(1, 2), List("One", "Two", "Three")) equals List((1, "One"), (2, "Two"), (-1, "Three")))
    assert(target.getZipAll(List(1, 2, 3), List("One", "Two")) equals List((1, "One"), (2, "Two"), (3, "?")))
  }

  test("LoopsAndCollections.IteratorNotes.getZipWithIndex") {
    val target = new IteratorNotes()
    assert(target.getZipWithIndex(List("One", "Two")) equals List(("One", 0), ("Two", 1)))
  }

  test("SameElements") {
    val xs = List("Manny", "Moe", "Jack")
    val ys = List("Manny", "Moe", "Jack")
    assert((xs sameElements ys) equals true)

    val xt = List("Manny", "Moe", "Jack")
    val yt = List("Manny", "Jack", "Moe")
    assert((xt sameElements yt) equals false)

    val xs1 = Set(3, 2, 1, 4, 5, 6, 7)
    val ys1 = Set(7, 2, 1, 4, 5, 6, 3)
    assert((xs1 sameElements ys1) equals true) //wha... see https://stackoverflow.com/questions/29008500/scala-sets-contain-the-same-elements-but-sameelements-returns-false
    //turns out for sets with more than 5 elements, a hash table is used and as a result they are sorted.

    val xt1 = Set(1, 2, 3)
    val yt1 = Set(3, 2, 1)
    assert((xt1 sameElements yt1) equals false)
    //under 5 elements so a hash table isn't used
  }
}
