package LoopsAndCollections

import org.scalatest.FunSuite

class LoopNotesTest extends FunSuite {
  test("LoopsAndSequences.LoopNotes.learnYield") {
    val target = new LoopNotes()
    assert(target.learnYield(4) equals (3, 1))
  }

  test("LoopsAndSequences.LoopNotes.listOfLists") {
    val target = new LoopNotes()
    assert(target.listofLists(1) equals 4)
    assert(target listofLists 1 equals 4) //shows how you can break out single parameter items by space
  }
}
