package Composition

import org.scalatest.FunSuite

class SimpleTest extends FunSuite {
  test("singleComp1") {
    assert(Simple.singleComp1(Simple.double)(2) equals 4)
    assert(Simple.singleComp1(Simple.triple)(2) equals 6)
  }
  test("singleComp2") {
    assert(Simple.singleComp2(Simple.double)(2) equals 4)
    assert(Simple.singleComp2(Simple.triple)(2) equals 6)
  }

  test("duoComp1") {
    assert(Simple.duoComp1(Simple.triple, Simple.addTwo)(2) equals 12)
    assert(Simple.duoComp1(Simple.addTwo, Simple.triple)(2) equals 8)
  }

  test("duoComp2") {
    assert(Simple.duoComp2(Simple.triple, Simple.addTwo)(2) equals 12)
    assert(Simple.duoComp2(Simple.addTwo, Simple.triple)(2) equals 8)
  }

  test("twoParamComp") {
    assert(Simple.twoParamComp(Simple.triple, Simple.multi)(3, 2) equals 18)
    assert(Simple.twoParamComp(Simple.triple, Simple.add)(3, 2) equals 15)
  }

  test("twoParamCompCrazy") {
    assert(Simple.twoParamCompCrazy(Simple.add, Simple.multi)(3, 2, 1) equals 7)
    assert(Simple.twoParamCompCrazy(Simple.multi, Simple.add)(3, 2, 1) equals 5)
  }
}
