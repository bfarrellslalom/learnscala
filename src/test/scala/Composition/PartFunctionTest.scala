package Composition

import org.scalatest.FunSuite

class PartFunctionTest extends FunSuite {
  test("Composition.PartFunction.evenOdder1") {
    val target = new PartFunction()
    assert(target.evenOdder1(4) equals 8)
    assert(target.evenOdder1(3) equals 9)
  }

  test("Composition.PartFunction.evenOdder2") {
    val target = new PartFunction()
    assert(target.evenOdder2(4) equals 8)
    assert(target.evenOdder2(3) equals 9)
  }
}
