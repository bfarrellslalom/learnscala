package Scope

import org.scalatest.FunSuite

class SimpleTest extends FunSuite {
  test("scopeExample") {
    assert(Simple.scopeExample(10) equals 25)
  }
}
