package Scope

import org.scalatest.FunSuite

object ObjectScopeTest extends FunSuite{
  test("ObjectScope") {
    assert(ObjectScope.Baz.y equals 3)
  }
}
