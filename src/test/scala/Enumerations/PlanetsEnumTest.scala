package Enumerations

import org.scalatest.FunSuite

class PlanetsEnumTest extends FunSuite{
  test("PlanetEnum.basic") {
    assert(PlanetsEnum.Mercury.id equals 0) //enums have an order, and can be iterated on
    assert(PlanetsEnum.Mercury.toString equals "Mercury") //uses reflection to get name
    assert(PlanetsEnum.Mercury equals PlanetsEnum.Mercury)
    assert(!PlanetsEnum.Mercury.equals((PlanetsEnum.Mars)))
  }
}
