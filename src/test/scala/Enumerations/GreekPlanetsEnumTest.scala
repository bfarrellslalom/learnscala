package Enumerations

import org.scalatest.FunSuite

class GreekPlanetsEnumTest extends FunSuite{
  test("PlanetEnum.basic") {
    assert(GreekPlanetsEnum.Mercury.id equals 1) //enums have an order, and can be iterated on
    assert(GreekPlanetsEnum.Mercury.toString equals "Hermes") //uses reflection to get name
    assert(GreekPlanetsEnum.Mercury equals GreekPlanetsEnum.Mercury)
    assert(!GreekPlanetsEnum.Mercury.equals(GreekPlanetsEnum.Mars))
  }
}
