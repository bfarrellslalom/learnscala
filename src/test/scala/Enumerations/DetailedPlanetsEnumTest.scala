package Enumerations

import org.scalatest.FunSuite

class DetailedPlanetsEnumTest extends FunSuite{
  test("PlanetEnum.basic") {
    assert(DetailedPlanentsEnum.Mercury.id equals 0) //enums have an order, and can be iterated on
    assert(DetailedPlanentsEnum.Mercury.toString equals "Mercury") //uses reflection to get name
    assert(DetailedPlanentsEnum.Mercury equals DetailedPlanentsEnum.Mercury)
    assert(!DetailedPlanentsEnum.Mercury.equals(DetailedPlanentsEnum.Mars))
    assert(DetailedPlanentsEnum.Venus.mass equals 4.869e+24)
  }
}
